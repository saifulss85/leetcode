const containsDuplicate = function(nums) {
  // return cheatCodeWay(nums);
  return longWay(nums);
};

function cheatCodeWay(nums) {
  const set = new Set(nums);
  return set.size < nums.length;
}

function longWay(nums) {
  const map = {};
  for (const num of nums) {
    const doesValueExist = map[num] != null;
    if (doesValueExist) return true;
    map[num] = 1;
  }
  return false;
}

module.exports = {
  containsDuplicate,
  test
};
