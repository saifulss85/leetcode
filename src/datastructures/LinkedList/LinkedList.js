const Node = require('./Node');

class LinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
  }

  insertFirst(data) {
    const newHead = new Node(data, this.head);

    if (this.head === null && this.tail === null) {
      this.tail = newHead;
    }

    this.head = newHead;
  }

  insertLast(data) {
    const newTail = new Node(data);

    if (this.tail === null) this.head = this.tail = newTail;
    else {
      this.tail.next = newTail;
      this.tail = newTail;
    }
  }

  insertAt(data, index) {
    const newNode = new Node(data);

    // special case: index requested is 0
    if (index === 0) {
      newNode.next = this.head;
      this.head = newNode;
      return;
    }

    let currentIndex = 1;
    let currentNode = this.head.next;
    let previousNode = this.head;
    while (currentNode) {   // loop starts at second element onwards
      if (currentIndex === index) {
        // special case: index requested is last available index (current tail)
        if (currentNode === this.tail) {
          currentNode.next = newNode;
          this.tail = newNode;
          return;
        }

        previousNode.next = newNode;
        newNode.next = currentNode;
        return;
      }

      // update pointer
      previousNode = currentNode;
      currentNode = currentNode.next;
      currentIndex++;
    }

    // special case: index requested is last available index + 1 (i.e. to be the new tail)
    if (index >= currentIndex) {
      previousNode.next = newNode;
      this.tail = newNode;
      return;
    }

    // special case: index requested is out of bounds
    throw new Error('Index requested is out of bounds');
  }

  size() {
    let count = 0;
    let currentNode = this.head;
    while (currentNode) {
      count++;
      if (currentNode instanceof Node) {
        currentNode = currentNode.next;
      }
    }

    return count;
  }

  getLast() {
    return this.tail;
  }

  getFirst() {
    return this.head;
  }

  getAt(index) {
    let count = 0;
    let currentNode = this.head;
    while (currentNode) {
      if (count++ === index) return currentNode;
      currentNode = currentNode.next;
    }
    return null;
  }

  removeFirst() {
    if (this.head === this.tail) {
      this.head = this.tail = null;
    } else {
      this.head = this.head.next;
    }
  }

  removeLast() {
    if (this.tail === this.head) this.head = this.tail = null;
    else {
      let currentNode = this.head;
      while (currentNode) {
        if (currentNode.next === this.tail) {
          this.tail = currentNode;
          currentNode.next = null;
          break;
        }
        currentNode = currentNode.next;
      }
    }
  }

  removeAt(index) {
    // special case: list has no nodes
    if (this.head === null) return;

    // special case: list has 1 node
    if (index === 0 && this.head === this.tail) {
      this.head = this.tail = null;
      return;
    }

    // special case: list has >1 nodes and node to remove is the first node
    if (index === 0) {
      this.head = this.head.next;
      return;
    }

    // special case: node to remove is the second last node
    // special case: node to remove is the last node
    // main case: list has 2 or more nodes
    let currentIndex = 1;
    let currentNode = this.head.next;
    let previousNode = this.head;
    let nextNode = undefined;
    while (currentNode) {   // loop starts at second element onwards
      nextNode = currentNode.next;
      if (currentIndex++ === index) {
        previousNode.next = nextNode;
        if (currentNode === this.tail) this.tail = previousNode;
      }

      // moving the pointer
      previousNode = currentNode;
      currentNode = nextNode;
    }
  }

  forEach(callback) {
    let currentNode = this.head;
    while (currentNode) {
      callback(currentNode);
      currentNode = currentNode.next;
    }
  }

  clear() {
    this.head = null;
    this.tail = null;
  }

  toString() {
    let result = '';
    let currentNode = this.head;
    while (currentNode) {
      result += currentNode.data;
      currentNode = currentNode.next;
    }

    return result;
  }

  * [Symbol.iterator]() {
    let node = this.head;
    while (node) {
      yield node;
      node = node.next;
    }
  }
}

module.exports = LinkedList;