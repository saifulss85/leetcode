class Node {
  constructor(data, nextData = null) {
    this.data = data;

    if (nextData instanceof Node) {
      this.next = nextData;
    } else if (nextData !== null) {
      this.next = new Node(nextData);
    }
  }
}

module.exports = Node;