const mergeTwoLists = require('./solution').mergeTwoLists;
const ListNode = require('./solution').ListNode;

describe('splice 2 linked-lists', () => {
  test('it can print 1-2-3', () => {
    const listNode1 = new ListNode(1);
    listNode1.next = new ListNode(2);
    listNode1.next.next = new ListNode(3);

    listNode1.print();
  });

  test('it returns 1-2-3-4 from array', () => {
    const elements = [1, 2, 3, 4];

    let pointer = new ListNode(null);
    let start = pointer;

    elements.forEach(element => {
      pointer.next = new ListNode(element);
      pointer = pointer.next;
    });

    start = start.next;

    start.print();
  });

  test('it returns 1-2-3-4', () => {
    const listNode1 = new ListNode(1);
    listNode1.next = new ListNode(3);

    const listNode2 = new ListNode(2);
    listNode2.next = new ListNode(4);

    const merged = mergeTwoLists(listNode1, listNode2);

    const expected = new ListNode(1);
    expected.next = new ListNode(2);
    expected.next.next = new ListNode(3);
    expected.next.next.next = new ListNode(4);

    expect(merged.val).toEqual(expected.val);
    expect(merged.next.val).toEqual(expected.next.val);
    expect(merged.next.next.val).toEqual(expected.next.next.val);
    expect(merged.next.next.next.val).toEqual(expected.next.next.next.val);
  });

  test('it returns 1-2', () => {
    const listNode1 = new ListNode(1);

    const listNode2 = new ListNode(2);

    const merged = mergeTwoLists(listNode1, listNode2);

    const expected = new ListNode(1);
    expected.next = new ListNode(2);

    expect(merged.val).toEqual(expected.val);
    expect(merged.next.val).toEqual(expected.next.val);
  });

  test('it returns 1-2 even if the 2 lists have different orders', () => {
    const listNode1 = new ListNode(2);

    const listNode2 = new ListNode(1);

    const merged = mergeTwoLists(listNode1, listNode2);

    const expected = new ListNode(1);
    expected.next = new ListNode(2);

    expect(merged.val).toEqual(expected.val);
    expect(merged.next.val).toEqual(expected.next.val);
  });

  test('it returns 1-2-4-5', () => {
    const listNode1 = new ListNode(5);

    const listNode2 = new ListNode(1);
    listNode2.next = new ListNode(2);
    listNode2.next.next = new ListNode(4);

    const merged = mergeTwoLists(listNode1, listNode2);

    const expected = new ListNode(1);
    expected.next = new ListNode(2);
    expected.next.next = new ListNode(4);
    expected.next.next.next = new ListNode(5);

    expect(merged.val).toEqual(expected.val);
    expect(merged.next.val).toEqual(expected.next.val);
    expect(merged.next.next.val).toEqual(expected.next.next.val);
    expect(merged.next.next.next.val).toEqual(expected.next.next.next.val);
  });

  test('it returns empty list for 2 empty lists', () => {
    const listNode1 = null;
    const listNode2 = null;

    const merged = mergeTwoLists(listNode1, listNode2);

    expect(merged).toEqual(null);
  });

  test('it returns 1 when one of the lists is empty', () => {
    const listNode1 = new ListNode(1);
    const listNode2 = null;

    const merged = mergeTwoLists(listNode1, listNode2);

    const expected = new ListNode(1);

    expect(merged.val).toEqual(expected.val);
  });
});