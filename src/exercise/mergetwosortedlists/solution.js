function ListNode(val) {
  this.val = val;
  this.next = null;

  this.print = function () {
    let result = '';
    let current = this;
    while (current !== null) {
      result += current.val + ' ';
      current = current.next;
    }
    console.log(result);
  };
}

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
const mergeTwoLists = function (l1, l2) {
  // while-loop as long as there are nodes in both
  // in each iteration, create a new node and add it to the result list
  // when either no longer has nodes, add the remainder of either list to the result list

  let resultPointer = new ListNode(null);
  let startNode = resultPointer;

  let pointer1 = l1;
  let pointer2 = l2;

  while (pointer1 && pointer2) {
    if (pointer1.val <= pointer2.val) {
      resultPointer.next = new ListNode(pointer1.val);

      pointer1 = pointer1.next;
      resultPointer = resultPointer.next;
    } else {
      resultPointer.next = new ListNode(pointer2.val);

      pointer2 = pointer2.next;
      resultPointer = resultPointer.next;
    }
  }

  while (pointer1) {
    resultPointer.next = new ListNode(pointer1.val);
    resultPointer = resultPointer.next;
    pointer1 = pointer1.next;
  }

  while (pointer2) {
    resultPointer.next = new ListNode(pointer2.val);
    resultPointer = resultPointer.next;
    pointer2 = pointer2.next;
  }

  startNode = startNode.next;

  return startNode;
};

module.exports = {
  mergeTwoLists,
  ListNode,
};