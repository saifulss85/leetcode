const functions = require('./index');

describe('calculateEffectiveValue', () => {
  it('returns 4 when given IV', () => {
    const effectiveValue = functions.calculateEffectiveValue('I', 'V');
    expect(effectiveValue).toBe(4);
  });

  it('returns 9 when given IX', () => {
    const effectiveValue = functions.calculateEffectiveValue('I', 'X');
    expect(effectiveValue).toBe(9);
  });

  it('returns 40 when given XL', () => {
    const effectiveValue = functions.calculateEffectiveValue('X', 'L');
    expect(effectiveValue).toBe(40);
  });

  it('returns 90 when given XC', () => {
    const effectiveValue = functions.calculateEffectiveValue('X', 'C');
    expect(effectiveValue).toBe(90);
  });

  it('returns 400 when given CD', () => {
    const effectiveValue = functions.calculateEffectiveValue('C', 'D');
    expect(effectiveValue).toBe(400);
  });

  it('returns 900 when given CM', () => {
    const effectiveValue = functions.calculateEffectiveValue('C', 'M');
    expect(effectiveValue).toBe(900);
  });
});

describe('isSensitiveLetter', () => {
  it('returns true when I', () => {
    const result = functions.isSensitiveLetter('I');
    expect(result).toBeTruthy();
  });

  it('returns true when X', () => {
    const result = functions.isSensitiveLetter('X');
    expect(result).toBeTruthy();
  });

  it('returns true when C', () => {
    const result = functions.isSensitiveLetter('C');
    expect(result).toBeTruthy();
  });

  it('returns false when D', () => {
    const result = functions.isSensitiveLetter('D');
    expect(result).toBeFalsy();
  });

  it('returns false when V', () => {
    const result = functions.isSensitiveLetter('V');
    expect(result).toBeFalsy();
  });
});

describe('isSpecialHit', () => {
  it('returns true when given I and V', () => {
    const result = functions.isSpecialHit('I', 'V');
    expect(result).toBeTruthy();
  });

  it('returns true when given I and X', () => {
    const result = functions.isSpecialHit('I', 'X');
    expect(result).toBeTruthy();
  });
});

describe('romanToInt', () => {
  it('returns 3 when given III', () => {
    const result = functions.romanToInt('III');
    expect(result).toBe(3);
  });

  it('returns 4 when given IV', () => {
    const result = functions.romanToInt('IV');
    expect(result).toBe(4);
  });

  it('returns 5 when given V', () => {
    const result = functions.romanToInt('V');
    expect(result).toBe(5);
  });

  it('returns 6 when given VI', () => {
    const result = functions.romanToInt('VI');
    expect(result).toBe(6);
  });

  it('returns 9 when given IX', () => {
    const result = functions.romanToInt('IX');
    expect(result).toBe(9);
  });

  it('returns 40 when given XL', () => {
    const result = functions.romanToInt('XL');
    expect(result).toBe(40);
  });

  it('returns 90 when given XC', () => {
    const result = functions.romanToInt('XC');
    expect(result).toBe(90);
  });
});