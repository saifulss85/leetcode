function romanToInt(s) {
  // traverse left to right, adding as you go
  // but encounter I, X, or C, need to check next letter

  // if I appears, check if next letter is a V or X
  // if it is, those 2 letters represent a number - calculate it
  // if not, then the letter just represents itself - proceed as per normal

  // same for X : L and C
  // same for C : D and M

  // take CXIII
  // first char is C - but next letter is not D or M, so we take it as 100
  // second char is X - but next letter is not L or C, so we take it as 10
  // 3rd, 4th and 5th are I with no adjacent V or X, so just add plainly

  let result = 0;

  const letters = s.split('');
  for (let i = 0; i < letters.length; i++) {
    const currentLetter = letters[i];

    let effectiveInt = getIntValue(currentLetter);
    if (isSensitiveLetter(currentLetter) && i !== letters.length - 1) {
      const nextLetter = letters[i + 1];
      if (isSpecialHit(currentLetter, nextLetter)) {
        effectiveInt = calculateEffectiveValue(currentLetter, nextLetter);
        i++;
      }
    }

    result += effectiveInt;
  }

  return result;
}

function isSpecialHit(currentLetter, nextLetter) {
  if (currentLetter === undefined || nextLetter === undefined) throw new Error('undefined arguments not allowed');

  if (currentLetter === 'I') {
    if (nextLetter === 'V') return true;
    if (nextLetter === 'X') return true;
  }

  if (currentLetter === 'X') {
    if (nextLetter === 'L') return true;
    if (nextLetter === 'C') return true;
  }

  if (currentLetter === 'C') {
    if (nextLetter === 'D') return true;
    if (nextLetter === 'M') return true;
  }

  return false;
}

function calculateEffectiveValue(currentLetter, nextLetter) {
  // if currentLetter and nextLetter are a "special hit"

  const baseValue = getIntValue(nextLetter);
  const valueToSubtract = getIntValue(currentLetter);
  return baseValue - valueToSubtract;
}

function isSensitiveLetter(letter) {
  if (letter === 'I') return true;
  if (letter === 'X') return true;
  if (letter === 'C') return true;
  return false;
}

function getIntValue(letter) {
  if (letter === 'I') return 1;
  if (letter === 'V') return 5;
  if (letter === 'X') return 10;
  if (letter === 'L') return 50;
  if (letter === 'C') return 100;
  if (letter === 'D') return 500;
  if (letter === 'M') return 1000;
}

module.exports = {
  romanToInt,
  calculateEffectiveValue,
  isSensitiveLetter,
  isSpecialHit,
  getIntValue,
};