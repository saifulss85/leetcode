/**
 * @param {number} N
 * @return {number}
 */
const primePalindrome = function (N) {
  let numberAsBigInt = BigInt(N) - BigInt(1);

  while (true) {
    numberAsBigInt = getNextPalindrome(BigInt(numberAsBigInt));
    const numberAsNumber = parseInt(numberAsBigInt.toString());
    const isPrime = isPrimeNumber(numberAsNumber);
    if (isPrime) return numberAsNumber;
  }
};

/**
 *
 * @param {bigint} number
 * @returns {bigint}
 */
function getNextPalindrome(number) {
  const incrementedNumber = typeof number === 'bigint' ? number + BigInt(1) : BigInt(number) + BigInt(1);

  if (incrementedNumber.toString().length === 1) return incrementedNumber;

  const leftSide = getLeftSideNumber(incrementedNumber);
  const palindromedLeft = getPalindromeAsString(leftSide);

  const rightSide = getRightSideNumberAsString(incrementedNumber);
  const comparison = compareTwoValues(BigInt(palindromedLeft), BigInt(rightSide));
  if (comparison === 0) {
    // the right side is already the palindromedLeft - return the incrementedNumber
    return incrementedNumber;
  }
  if (comparison === 1) {
    // this means the right side can naturally increment to the palindromedLeft,
    // so you can just return the leftSideWithBorder concatenated with the palindromedLeft
    const leftSideWithBorder = getLeftSideNumberWithBorder(incrementedNumber);
    const leftAsString = leftSideWithBorder.toString();
    const combined = leftAsString + palindromedLeft;
    return BigInt(combined);
  }
  if (comparison === -1) {
    // this means the right side is already too far advanced compared to the palindromedLeft,
    // you need to take the leftSideWithBorder, increment by 1, then return this new number concatenated with
    // the leftSide's palindrome - this is the answer
    const leftWithBorder = getLeftSideNumberWithBorder(incrementedNumber);
    const incrementedLeftWithBorder = leftWithBorder + BigInt(1);
    const newWhole = BigInt(incrementedLeftWithBorder.toString() + getRightSideNumberAsString(incrementedNumber));

    const newLeft = getLeftSideNumber(newWhole);
    const palindromedNewLeft = getPalindromeAsString(newLeft);
    return BigInt(incrementedLeftWithBorder.toString() + palindromedNewLeft.toString());
  }
}

/**
 *
 * @param {bigint} number
 */
function getLeftSideNumber(number) {
  const numberAsText = number.toString();
  const numCharsInLeftSide = Math.floor(numberAsText.length / 2);
  return BigInt(numberAsText.slice(0, numCharsInLeftSide));
}

/**
 *
 * @param {bigint} number
 * @returns {bigint}
 */
function getLeftSideNumberWithBorder(number) {
  const numberAsText = number.toString();
  const hasOddNumChars = numberAsText.length % 2 === 1;

  const left = getLeftSideNumber(number);

  // should return the left side only, if it's an even-digited number
  // else, return the left side together with the border number (since it's an odd-digited number)
  if (hasOddNumChars) {
    const middleChar = numberAsText.charAt(Math.floor(numberAsText.length / 2));
    return BigInt(left.toString() + middleChar);
  } else {
    return BigInt(left.toString());
  }
}

/**
 *
 * @param {bigint} number
 * @returns {string}
 */
function getRightSideNumberAsString(number) {
  const numberAsText = number.toString();
  const numCharsInRightSide = Math.floor(numberAsText.length / 2);
  return numberAsText.slice(numberAsText.length - numCharsInRightSide);
}

/**
 *
 * @param {bigint} number
 * @returns {string}
 */
function getPalindromeAsString(number) {
  const numberAsText = number.toString();
  return numberAsText.split("").reverse().join("");
}

/**
 *
 * @param {bigint} number1
 * @param {bigint} number2
 * @returns {number}
 */
function compareTwoValues(number1, number2) {
  if (number1 < number2) return -1;
  if (number1 === number2) return 0;
  if (number1 > number2) return 1;
}

/**
 *
 * @param {number} number
 * @returns {boolean}
 */
function isPrimeNumber(number) {
  if (number < 2) return false;

  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      // console.log(i);
      return false;
    }
  }

  return true;
}

module.exports = {
  primePalindrome,
  isPrimeNumber,
};