const primePalindrome = require('./solution').primePalindrome;
const isPrimeNumber = require('./solution').isPrimeNumber;

describe('primePalindrome', () => {
  test('it, when given 2, returns 2', () => {
    expect(primePalindrome(2)).toBe(2);
  });

  test('it, when given 6, returns 7', () => {
    expect(primePalindrome(6)).toBe(7);
  });

  test('it, when given 8, returns 11', () => {
    expect(primePalindrome(8)).toBe(11);
  });
});

describe('isPrimeNumber', () => {
  test('it returns true for 2', () => {
    expect(isPrimeNumber(2)).toBeTruthy();
  });

  test('it returns true for 479', () => {
    expect(isPrimeNumber(479)).toBeTruthy();
  });

  test('it returns false for 477', () => {
    expect(isPrimeNumber(477)).toBeFalsy();
  });

  test('it returns false for 481', () => {
    expect(isPrimeNumber(481)).toBeFalsy();
  });

  test('it returns false for 9', () => {
    expect(isPrimeNumber(9)).toBeFalsy();
  });

  test('it returns false for 49', () => {
    expect(isPrimeNumber(49)).toBeFalsy();
  });
});