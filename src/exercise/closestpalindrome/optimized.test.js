const getRightSideNumber = require('./optimized').getRightSideNumber;
const getLeftSideNumber = require('./optimized').getLeftSideNumber;
const getLeftSideNumberWithBorder = require('./optimized').getLeftSideNumberWithBorder;
const nearestPalindromic = require('./optimized').nearestPalindromic;
const getNextPalindrome = require('./optimized').getNextPalindrome;
const getPrevPalindrome = require('./optimized').getPrevPalindrome;

describe('nearestPalindromic', () => {
  test('it, when given 1, returns 0', () => {
    expect(nearestPalindromic(BigInt('1'))).toBe('0');
  });

  test('it, when given 1111, returns 1001', () => {
    expect(nearestPalindromic(BigInt('1111'))).toBe('1001');
  });

  test('it, when given 1234, returns 1221', () => {
    expect(nearestPalindromic(BigInt('1234'))).toBe('1221');
  });

  test('it, when given 999, returns 1001', () => {
    expect(nearestPalindromic(BigInt('999'))).toBe('1001');
  });

  test('it, when given 12932, returns 12921', () => {
    expect(nearestPalindromic(BigInt('12932'))).toBe('12921');
  });

  test('it, when given 807045053224792883, returns 12921', () => {
    const result = nearestPalindromic(BigInt('807045053224792883'));
    // console.log(result);
    expect(result).toBe('807045053350540708');
  });

  test('it, when given 9007199254740991, returns 9007199339917009', () => {
    expect(nearestPalindromic(BigInt('9007199254740991'))).toBe('9007199229917009');  // larger counterpart is 9007199339917009
  });

  test('it can take non-bigint input', () => {
    expect(nearestPalindromic('1')).toBe('0');
  });

  test('it, when given "11", returns "9"', () => {
    expect(nearestPalindromic('11')).toBe('9');
  });

  test('it, when given "10001", returns "9999"', () => {
    expect(nearestPalindromic('10001')).toBe('9999');
  });
});

describe('getPrevPalindrome', () => {
  test('it, when given 1111, returns 1001', () => {
    const prevPalindrome = getPrevPalindrome(BigInt(1111));
    // console.log(prevPalindrome);
    expect(prevPalindrome).toBe(BigInt(1001));
  });

  test('it, when given 1221, returns 1111', () => {
    const prevPalindrome = getPrevPalindrome(BigInt(1221));
    console.log(prevPalindrome);
    expect(prevPalindrome).toBe(BigInt(1111));
  });

  test('it, when given 807045053224792883, returns 807045052250540708', () => {
    const prevPalindrome = getPrevPalindrome(BigInt('807045053224792883'));
    console.log(prevPalindrome);
    expect(prevPalindrome).toBe(BigInt('807045052250540708'));
  });
});

describe('getNextPalindrome', () => {
  test('it, when given 1111, returns 1221', () => {
    const nextPalindrome = getNextPalindrome(BigInt('1111'));
    console.log(nextPalindrome);
    expect(nextPalindrome).toBe(BigInt('1221'));
  });

  test('it, when given 807045053224792883, returns 807045053350540708', () => {
    const nextPalindrome = getNextPalindrome(BigInt('807045053224792883'));
    console.log(nextPalindrome);
    expect(nextPalindrome).toBe(BigInt('807045053350540708'));
  });
});

describe('getLeftSideNumber', () => {
  test('it, when given 9990999, returns 999', () => {
    expect(getLeftSideNumber(BigInt(9990999))).toEqual(BigInt(999));
  });

  test('it, when given 999999, returns 999', () => {
    expect(getLeftSideNumber(BigInt(999999))).toEqual(BigInt(999));
  });

  test('it, when given largest number in JS plus 2, returns 9990', () => {
    const result = getLeftSideNumber(BigInt(Number.MAX_SAFE_INTEGER) + BigInt(2));
    expect(result).toEqual(BigInt(90071992));
  });
});

describe('getLeftSideNumberWithBorder', () => {
  test('it, when given 9990999, returns 9990', () => {
    expect(getLeftSideNumberWithBorder(BigInt(9990999))).toEqual(BigInt(9990));
  });

  test('it, when given 999999, returns 999', () => {
    expect(getLeftSideNumberWithBorder(BigInt(999999))).toEqual(BigInt(999));
  });

  test('it, when given largest number in JS plus 2, returns 9990', () => {
    const result = getLeftSideNumberWithBorder(BigInt(Number.MAX_SAFE_INTEGER) + BigInt( 2));
    expect(result).toEqual(BigInt(90071992));
  });
});

describe('getRightSideNumber', () => {
  test('it, when given 9990999, returns 9990', () => {
    expect(getRightSideNumber(BigInt(9990999))).toEqual(BigInt(999));
  });

  test('it, when given 999999, returns 999', () => {
    expect(getRightSideNumber(BigInt(999999))).toEqual(BigInt(999));
  });

  test('it, when given largest number in JS plus 2, returns 9990', () => {
    const result = getRightSideNumber(BigInt(Number.MAX_SAFE_INTEGER) + BigInt(2));
    // console.log(Number.MAX_SAFE_INTEGER);
    // console.log(result);
    expect(result).toEqual(BigInt(54740993));
  });
});

