const nearestPalindromic = require('./bruteforce').nearestPalindromic;
const getNextPalindrome = require('./bruteforce').getNextPalindrome;
const getPrevPalindrome = require('./bruteforce').getPrevPalindrome;
const isNumberPalindromic = require('./bruteforce').isNumberPalindromic;

describe('nearestPalindromic', () => {
  test('it, when given 1, returns 0', () => {
    expect(nearestPalindromic(BigInt('1'))).toBe('0');
  });

  test('it, when given 1111, returns 1001', () => {
    expect(nearestPalindromic(BigInt('1111'))).toBe('1001');
  });

  test('it, when given 1234, returns 1221', () => {
    expect(nearestPalindromic(BigInt('1234'))).toBe('1221');
  });

  test('it, when given 999, returns 1001', () => {
    expect(nearestPalindromic(BigInt('999'))).toBe('1001');
  });

  test('it, when given 12932, returns 12921', () => {
    expect(nearestPalindromic(BigInt('12932'))).toBe('12921');
  });

  test('it, when given 807045053224792883, returns 12921', () => {
    expect(nearestPalindromic(BigInt('807045053224792883'))).toBe('807045053350540708');
  });

  test('it, when given 9007199254740991, returns 9007199339917009', () => {
    expect(nearestPalindromic(BigInt('9007199254740991'))).toBe('9007199229917009');  // larger counterpart is 9007199339917009
  });
});

describe('getPrevPalindrome', () => {
  test('it, when given 1111, returns 1001', () => {
    expect(getPrevPalindrome(BigInt('1111'))).toBe(BigInt('1001'));
  });
});

describe('getNextPalindrome', () => {
  test('it, when given 1111, returns 1221', () => {
    expect(getNextPalindrome(BigInt('1111'))).toBe(BigInt('1221'));
  });
});

describe('isNumberPalindromic', () => {
  test('it passes for 1001', () => {
    expect(isNumberPalindromic(BigInt('1001'))).toBeTruthy();
  });

  test('it passes for 10001', () => {
    expect(isNumberPalindromic(BigInt('10001'))).toBeTruthy();
  });

  test('it passes for palindrome just over largest JS number', () => {
    expect(isNumberPalindromic(BigInt('9111111111111119'))).toBeTruthy();
  });

  test('it fails for non-palindrome bigger than largest JS number', () => {
    expect(isNumberPalindromic(BigInt('9111111111111118'))).toBeFalsy();
  });
});