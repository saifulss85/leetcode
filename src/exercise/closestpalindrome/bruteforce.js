/**
 * @param {bigint} n
 * @return {string}
 */
const nearestPalindromic = function(n) {
  // take the number, keep adding 1 to it, then check if it's a palindrome
  const prevPalindrome = getPrevPalindrome(n);
  const nextPalindrome = getNextPalindrome(n);

  const scalarPrev = n - prevPalindrome;
  const scalarNext = nextPalindrome - n;

  if (scalarPrev <= scalarNext) return prevPalindrome.toString();
  else return nextPalindrome.toString();
};

function getPrevPalindrome(number) {
  let currentNumber = number;
  while (true) {
    currentNumber--;
    if (isNumberPalindromic(currentNumber)) break;
  }

  return currentNumber;
}

function getNextPalindrome(number) {
  let currentNumber = number;
  while (true) {
    currentNumber++;
    if (isNumberPalindromic(currentNumber)) break;
  }

  return currentNumber;
}

/**
 *
 * @param {bigint} number
 * @returns {boolean}
 */
function isNumberPalindromic(number) {
  const numberAsString = number + '';
  let left = 0;
  let right = numberAsString.length - 1;

  while (left < right) {
    const leftChar = numberAsString.charAt(left);
    const rightChar = numberAsString.charAt(right);
    if (leftChar !== rightChar) {
      return false;
    }

    left++;
    right--;
  }

  return true;
}

module.exports = {
  nearestPalindromic,
  isNumberPalindromic,
  getPrevPalindrome,
  getNextPalindrome,
};