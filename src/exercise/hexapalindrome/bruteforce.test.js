const nextSmallestHexPalindrome = require('./bruteforce').nextSmallestHexPalindrome;

describe('nextSmallestHexPalindrome', () => {
  test('it, when given ffe, returns fff', () => {
    expect(nextSmallestHexPalindrome('ffe')).toEqual('fff');
  });

  test('it, when given largest JS number, returns correctly', () => {
    const largestNumber = Number.MAX_SAFE_INTEGER;
    const largestNumberAsHexString = largestNumber.toString(16);

    expect(nextSmallestHexPalindrome(largestNumberAsHexString)).toEqual('20000000000002');
  });

  test('it, when given largest JS number + 1, returns correctly', () => {
    const largestNumberAsBigInt = BigInt(Number.MAX_SAFE_INTEGER);
    const largestNumberAsBigIntPlus1 = largestNumberAsBigInt + BigInt(1);
    const largestNumberAsHexString = largestNumberAsBigIntPlus1.toString(16);

    console.log(largestNumberAsHexString);
    expect(nextSmallestHexPalindrome(largestNumberAsHexString)).toEqual('20000000000002');
  });

  test('it, when given largest JS number + 3, returns correctly', () => {
    const largestNumberAsBigInt = BigInt(Number.MAX_SAFE_INTEGER);
    const largestNumberAsBigIntPlus1 = largestNumberAsBigInt + BigInt(3);
    const largestNumberAsHexString = largestNumberAsBigIntPlus1.toString(16); // 20000000000002

    expect(nextSmallestHexPalindrome(largestNumberAsHexString)).toEqual('20000011000002');
  });

  test('it returns 2aa2 when given 2999', () => {
    expect(nextSmallestHexPalindrome('2999')).toEqual('2aa2');
  });

  test('it returns 111 when given 105', () => {
    expect(nextSmallestHexPalindrome('105')).toEqual('111');
  });

  test('it returns 3ff3 when given 3f97', () => {
    expect(nextSmallestHexPalindrome('3f97')).toEqual('3ff3');
  });

  test('it returns 2 when given 1', () => {
    expect(nextSmallestHexPalindrome('1')).toEqual('2');
  });

  test('it returns 10001 when given ffff', () => {
    expect(nextSmallestHexPalindrome('ffff')).toEqual('10001');
  });

  test('it returns 33 when given 23', () => {
    expect(nextSmallestHexPalindrome('23')).toEqual('33');
  });

  test('it returns a99a when given a999', () => {
    expect(nextSmallestHexPalindrome('a999')).toEqual('a99a');
  });

  test('it returns fff when given ffe', () => {
    expect(nextSmallestHexPalindrome('ffe')).toEqual('fff');
  });

  test('it returns fffff when given ffffe', () => {
    expect(nextSmallestHexPalindrome('ffffe')).toEqual('fffff');
  });

  test('it returns ffffff0ffffff when given ffffff0fffeff', () => {
    expect(nextSmallestHexPalindrome('ffffff0fffffe')).toEqual('ffffff0ffffff');
  });

  test('it returns fffffff0fffffff when given ffffff0fffffe', () => {
    expect(nextSmallestHexPalindrome('ffffff0fffffe')).toEqual('ffffff0ffffff');
  });
});