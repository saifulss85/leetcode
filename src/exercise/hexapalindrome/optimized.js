// step 1: add 1 to the number, whatever it is
// step 2: check the left-side number, then check to see what the right-side number is
// step 3: if the palindrome of left-side number is numerically smaller than the current right-side number,
// that means the current is already ahead of that target number,
// which means the left-side number (plus middle, if odd-numbered digits) needs to increment by 1
// step 4: with the incremented left-side number, find the palindrome and just make the right side be that
const nextSmallestHexPalindrome = hexNumber => {
  const hexNumberIncremented = add1ToHexNumber(hexNumber);

  if (hexNumberIncremented.length === 1) return hexNumberIncremented;

  const leftSide = getLeftSideNumber(hexNumberIncremented);
  const palindromedLeft = getPalindromeOfHex(leftSide);

  const rightSide = getRightSideNumber(hexNumberIncremented);
  const comparison = compare2HexValues(palindromedLeft, rightSide);
  if (comparison === 0) {
    // the right side is already the palindromedLeft - return the hexNumberIncremented
    return hexNumberIncremented;
  }
  if (comparison === 1) {
    // this means the right side can naturally increment to the palindromedLeft,
    // so you can just return the leftSideWithBorder concatenated with the palindromedLeft
    const leftSideWithBorder = getLeftSideNumberWithBorder(hexNumberIncremented);
    return leftSideWithBorder + palindromedLeft;
  }
  if (comparison === -1) {
    // this means the right side is already too far advanced compared to the palindromedLeft,
    // you need to take the leftSideWithBorder, increment by 1, then return this new number concatenated with
    // the leftSide's palindrome - this is the answer
    const leftWithBorder = getLeftSideNumberWithBorder(hexNumberIncremented);
    const incrementedLeftWithBorder = add1ToHexNumber(leftWithBorder);
    const newWhole = incrementedLeftWithBorder + getRightSideNumber(hexNumberIncremented);

    const newLeft = getLeftSideNumber(newWhole);
    const palindromedNewLeft = getPalindromeOfHex(newLeft);
    return incrementedLeftWithBorder + palindromedNewLeft;
  }
};

function add1ToHexNumber(hexNumber) {
  return (parseInt(hexNumber, 16) + 1).toString(16);
}

/**
 *
 * @param {string} hexNumber
 * @param {boolean} withBorderNumber
 */
function getLeftSideNumber(hexNumber) {
  const numCharsInLeftSide = Math.floor(hexNumber.length / 2);
  return hexNumber.slice(0, numCharsInLeftSide);
}

/**
 *
 * @param {string} hexNumber
 */
function getLeftSideNumberWithBorder(hexNumber) {
  const hasOddNumChars = hexNumber.length % 2 === 1;

  const left = getLeftSideNumber(hexNumber);

  // should return the left side only, if it's an even-digited number
  // else, return the left side together with the border number (since it's an odd-digited number)
  if (hasOddNumChars) {
    const middleChar = hexNumber.charAt(Math.floor(hexNumber.length / 2));
    return left + middleChar;
  } else {
    return left;
  }
}

/**
 *
 * @param {string} hexNumber
 */
function getRightSideNumber(hexNumber) {
  const numCharsInRightSide = Math.floor(hexNumber.length / 2);
  return hexNumber.slice(hexNumber.length - numCharsInRightSide);
}

function getPalindromeOfHex(hexNumber) {
  return hexNumber.split("").reverse().join("");
}

function compare2HexValues(hexNumber1, hexNumber2) {
  const number1 = parseInt(hexNumber1, 16);
  const number2 = parseInt(hexNumber2, 16);

  if (number1 < number2) return -1;
  if (number1 === number2) return 0;
  if (number1 > number2) return 1;
}

module.exports = {
  nextSmallestHexPalindrome,
  add1ToHexNumber,
  getLeftSideNumber,
  getRightSideNumber,
  getPalindromeOfHex,
  compare2HexValues,
  getLeftSideNumberWithBorder,
};