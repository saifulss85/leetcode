const getLeftSideNumberWithBorder = require('./optimized').getLeftSideNumberWithBorder;
const compare2HexValues = require('./optimized').compare2HexValues;
const getPalindromeOfHex = require('./optimized').getPalindromeOfHex;
const getRightSideNumber = require('./optimized').getRightSideNumber;
const getLeftSideNumber = require('./optimized').getLeftSideNumber;
const add1ToHexNumber = require('./optimized').add1ToHexNumber;
const nextSmallestHexPalindrome = require('./optimized').nextSmallestHexPalindrome;

describe('nextSmallestHexaPalindrome', () => {
  test('it returns 2aa2 when given 2999', () => {
    expect(nextSmallestHexPalindrome('2999')).toEqual('2aa2');
  });

  test('it returns 111 when given 105', () => {
    expect(nextSmallestHexPalindrome('105')).toEqual('111');
  });

  test('it returns 3ff3 when given 3f97', () => {
    expect(nextSmallestHexPalindrome('3f97')).toEqual('3ff3');
  });

  test('it returns 2 when given 1', () => {
    expect(nextSmallestHexPalindrome('1')).toEqual('2');
  });

  test('it returns 10001 when given ffff', () => {
    expect(nextSmallestHexPalindrome('ffff')).toEqual('10001');
  });

  test('it returns 33 when given 23', () => {
    expect(nextSmallestHexPalindrome('23')).toEqual('33');
  });

  test('it returns a99a when given a999', () => {
    expect(nextSmallestHexPalindrome('a999')).toEqual('a99a');
  });

  test('it returns fff when given ffe', () => {
    expect(nextSmallestHexPalindrome('ffe')).toEqual('fff');
  });

  test('it returns fffff when given ffffe', () => {
    expect(nextSmallestHexPalindrome('ffffe')).toEqual('fffff');
  });

  test('it returns ffffff0ffffff when given ffffff0fffeff', () => {
    expect(nextSmallestHexPalindrome('ffffff0fffffe')).toEqual('ffffff0ffffff');
  });

  test('it returns fffffff0fffffff when given ffffff0fffffe', () => {
    expect(nextSmallestHexPalindrome('ffffff0fffffe')).toEqual('ffffff0ffffff');
  });

  test('it, when given largest JS number, returns correctly', () => {
    const largestNumber = Number.MAX_SAFE_INTEGER;
    const largestNumberAsHexString = largestNumber.toString(16);

    expect(nextSmallestHexPalindrome(largestNumberAsHexString)).toEqual('20000000000002');
  });

  test('it, when given largest JS number + 1, returns correctly', () => {
    const largestNumberAsBigInt = BigInt(Number.MAX_SAFE_INTEGER);
    const largestNumberAsBigIntPlus1 = largestNumberAsBigInt + BigInt(1);
    const largestNumberAsHexString = largestNumberAsBigIntPlus1.toString(16);

    console.log(largestNumberAsHexString);
    expect(nextSmallestHexPalindrome(largestNumberAsHexString)).toEqual('20000000000002');
  });

  test('it, when given largest JS number + 3, returns correctly', () => {
    const largestNumberAsBigInt = BigInt(Number.MAX_SAFE_INTEGER);
    const largestNumberAsBigIntPlus1 = largestNumberAsBigInt + BigInt(3);
    const largestNumberAsHexString = largestNumberAsBigIntPlus1.toString(16); // 20000000000002

    expect(nextSmallestHexPalindrome(largestNumberAsHexString)).toEqual('20000011000002');
  });
});

describe('add1ToHexNumber', () => {
  test('it returns a when given 9', () => {
    expect(add1ToHexNumber('9')).toEqual('a');
  });

  test('it returns 100 when given ff', () => {
    expect(add1ToHexNumber('ff')).toEqual('100');
  });
});

describe('getLeftSideNumber', () => {
  test('it returns 29 when given 2999', () => {
    expect(getLeftSideNumber('2999')).toEqual('29');
  });

  test('it returns 29 when given 29999', () => {
    expect(getLeftSideNumber('29999')).toEqual('29');
  });

  test('it returns 1 when given 105', () => {
    expect(getLeftSideNumber('105')).toEqual('1');
  });
});

describe('getRightSideNumber', () => {
  test('it returns 99 when given 2999', () => {
    expect(getRightSideNumber('2999')).toEqual('99');
  });

  test('it returns 99 when given 29999', () => {
    expect(getRightSideNumber('29999')).toEqual('99');
  });

  test('it returns 5 when given 105', () => {
    expect(getRightSideNumber('105')).toEqual('5');
  });
});

describe('getPalindromeOfHex', () => {
  test('it returns 123 when given 321', () => {
    expect(getPalindromeOfHex('321')).toEqual('123');
  });

  test('it returns fff when given fff', () => {
    expect(getPalindromeOfHex('fff')).toEqual('fff');
  });

  test('it returns f when given f', () => {
    expect(getPalindromeOfHex('f')).toEqual('f');
  });

  test('it returns 92 when given 29', () => {
    expect(getPalindromeOfHex('29')).toEqual('92');
  });
});

describe('compare2HexValues', () => {
  test('it returns 0 when given fff and fff', () => {
    expect(compare2HexValues('fff', 'fff')).toEqual(0);
  });

  test('it returns -1 when given ffe and fff', () => {
    expect(compare2HexValues('ffe', 'fff')).toEqual(-1);
  });

  test('it returns 1 when given fff and ffe', () => {
    expect(compare2HexValues('fff', 'ffe')).toEqual(1);
  });
});

describe('getLeftSideNumberWithBorder', () => {
  test('it returns 22 when given 2234', () => {
    expect(getLeftSideNumberWithBorder('2234')).toEqual('22');
  });

  test('it returns 222 when given 22234', () => {
    expect(getLeftSideNumberWithBorder('22234')).toEqual('222');
  });
});

