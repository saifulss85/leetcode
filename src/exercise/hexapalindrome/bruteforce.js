function nextSmallestHexPalindrome(hexString) {
  // first, we convert the hexString to a BigInt
  // then in a while (true) loop, we immediately add 1 to the BigInt, convert it to a hexString, then check if it's a palindrome
  // if palindrome, break
  // else, continue until break

  // while (true), add 1 to number then check if new number is palindrome
  let currentNumber = BigInt(parseInt(hexString, 16));
  while (true) {
    currentNumber++;
    const currentNumberAsHexString = currentNumber.toString(16);
    if (isPalindrome(currentNumberAsHexString)) break;
  }

  return currentNumber.toString(16);
}

/**
 *
 * @param {string} hexString
 */
function isPalindrome(hexString) {
  if (hexString.length === 1) return true;

  let left = 0;
  let right = hexString.length - 1;

  while (left < right) {
    if (hexString.charAt(left) !== hexString.charAt(right)) return false;

    left++;
    right--;
  }

  return true;
}

module.exports = {
  nextSmallestHexPalindrome,
  isPalindrome,
};