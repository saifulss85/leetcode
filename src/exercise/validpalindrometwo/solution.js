/**
 * @param {string} s
 * @return {boolean}
 */
const validPalindrome = function(s) {
  // have a function that accepts string, left and right
  // have left and right pointer
  // while left < right, check charAt left and right
  // if chars are same, move on
  // if chars not same, make 2 calls to the function:
  // 1) with string, left+1 and right
  // 2) with string, left and right-1

  let left = 0;
  let right = s.length - 1;
  while (left < right) {
    if (s.charAt(left) !== s.charAt(right)) {
      const leftDisplaceIsPalindrome = isPalindrome(s, left + 1, right);
      const rightDisplaceIsPalindrome = isPalindrome(s, left, right - 1);
      return leftDisplaceIsPalindrome || rightDisplaceIsPalindrome;
    }

    left++;
    right--;
  }

  return true;
};

/**
 * Checks if a string is a palindrome from the given positions left and right inwards
 *
 * @param {string} string
 * @param {number} left
 * @param {number} right
 */
function isPalindrome(string, left, right) {
  let currentLeft = left;
  let currentRight = right;
  while (currentLeft < currentRight) {
    if (string.charAt(currentLeft) !== string.charAt(currentRight)) {
      return false;
    }

    currentLeft++;
    currentRight--;
  }

  return true;
}

module.exports = {
  validPalindrome,
  isPalindrome,
};