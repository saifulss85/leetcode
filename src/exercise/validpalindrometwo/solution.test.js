const validPalindrome = require('./solution').validPalindrome;
const isPalindrome = require('./solution').isPalindrome;

describe('validPalindrome', () => {
  test('it passes for abcdcba', () => {
    const string = 'abcdcba';
    expect(validPalindrome(string)).toBeTruthy();
  });

  test('it passes for abcdecba', () => {
    const string = 'abcdecba';
    expect(validPalindrome(string)).toBeTruthy();
  });

  test('it fails for abcedcba', () => {
    const string = 'abcedcba';
    expect(validPalindrome(string)).toBeTruthy();
  });

  test('it fails for aaaxyzaaa', () => {
    const string = 'aaaxyzaaa';
    expect(validPalindrome(string)).toBeFalsy();
  });
});

describe('isPalindrome', () => {
  test('it passes for abcdcba', () => {
    const string = 'abcdcba';
    expect(isPalindrome(string, 0, string.length - 1)).toBeTruthy();
  });

  test('it fails for abcedcba', () => {
    const string = 'abcedcba';
    expect(isPalindrome(string, 0, string.length - 1)).toBeFalsy();
  });
});