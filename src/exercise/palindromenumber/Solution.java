package exercise.palindromenumber;

public class Solution {
    public static void main(String[] args) {
        int number = 1234444321;

        Solution solution = new Solution();
        System.out.println(solution.isPalindrome(number));
    }

//    public boolean isPalindrome(int x) {
//        // 1234321
//        // convert to string, use StringBuilder to reverse the string, compare
//
//        String original = String.valueOf(x);
//        StringBuilder stringBuilder = new StringBuilder(original);
//        String reversed = stringBuilder.reverse().toString();
//
//        return reversed.equalsIgnoreCase(original);
//    }

    public boolean isPalindrome(int x) {
        if (x < 0) return false;

        int current = x;
        int reversedNumber = 0;
        while (current != 0) {
            int digit = current % 10;
            reversedNumber = reversedNumber * 10 + digit;
            current = current / 10;
        }
//        System.out.println(reversedNumber);

        return x == reversedNumber;
    }
}