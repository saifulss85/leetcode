package exercise.twosum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static void main(String[] args) {
        int[] nums = new int[]{2, 7, 11, 15};
        int target = 9;

        Solution solution = new Solution();
        int[] result = solution.twoSum(nums, target);

        System.out.println(Arrays.toString(result));
    }

//    public int[] twoSum(int[] nums, int target) {
//        int[] result = new int[2];
//
//        for (int referenceIndex = 0; referenceIndex < nums.length; referenceIndex++) {
//            for (int comparisonIndex = referenceIndex + 1; comparisonIndex < nums.length; comparisonIndex++) {
//                if (nums[referenceIndex] + nums[comparisonIndex] == target) {
//                    result[0] = referenceIndex;
//                    result[1] = comparisonIndex;
//                }
//            }
//        }
//
//        return result;
//    }

    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];

        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], i);
            map.put(nums[i], i);
        }
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (map.containsKey(complement) && map.get(complement) != i) {
                return new int[] { i, map.get(complement) };
            }
        }

        return result;
    }
}