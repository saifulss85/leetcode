import { inorderTraversal, postorderTraversal, preorderTraversal, TreeNode } from './index';

const tree1 = new TreeNode(1);
const tree2 = new TreeNode(1);

describe('binary trees', () => {
  beforeEach(() => {
    tree1.left = null;
    tree1.right = new TreeNode(2);
    tree1.right.left = new TreeNode(3);

    tree2.left = new TreeNode(2);
    tree2.left.right = new TreeNode(4);
    tree2.left.right.left = new TreeNode(5);
    tree2.right = new TreeNode(3);
    tree2.right.left = new TreeNode(6);
    tree2.right.left.left = new TreeNode(7);
    tree2.right.left.left.right = new TreeNode(8);
    tree2.right.left.left.right.left = new TreeNode(9);
    tree2.right.left.left.right.right = new TreeNode(10);
  });

  describe('pre order traversal', () => {
    test('it can pass with 3 elements', () => {
      expect(preorderTraversal(tree1)).toEqual([1, 2, 3]);
    });

    test('it can pass with 10 elements', () => {
      expect(preorderTraversal(tree2)).toEqual([1, 2, 4, 5, 3, 6, 7, 8, 9, 10]);
    });
  });

  describe('in order traversal', () => {
    test('it can pass with 3 elements', () => {
      expect(inorderTraversal(tree1)).toEqual([1, 3, 2]);
    });
  });

  describe('post order traversal', () => {
    test('it can pass with 3 elements', () => {
      expect(postorderTraversal(tree1)).toEqual([3, 2, 1]);
    });
  });
});