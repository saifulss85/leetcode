export function TreeNode(val) {
  this.val = val;
  this.left = this.right = null;
}

export const preorderTraversal = function (root) {
  if (root === null) return [];

  const left = preorderTraversal(root.left);
  const right = preorderTraversal(root.right);

  return [root.val, ...left, ...right];
};

export const inorderTraversal = function(root) {
  if (root === null) return [];

  const left = inorderTraversal(root.left);
  const right = inorderTraversal(root.right);

  return [...left, root.val, ...right];
};

export const postorderTraversal = function(root) {
  if (root === null) return [];

  const left = postorderTraversal(root.left);
  const right = postorderTraversal(root.right);

  return [...left, ...right, root.val];
};