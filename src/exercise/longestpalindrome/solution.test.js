const longestPalindrome = require('./solution').longestPalindrome;

describe('longest palindrome', () => {
  test('it returns 0 for empty string', () => {
    expect(longestPalindrome('')).toEqual(0);
  });

  test('it returns 0 for null or undefined', () => {
    expect(longestPalindrome(null)).toEqual(0);
    expect(longestPalindrome(undefined)).toEqual(0);
  });

  test('it returns 1 for 1 character string', () => {
    expect(longestPalindrome('s')).toEqual(1);
  });

  test('it returns 1 for 2 character string of differing case', () => {
    // expect(longestPalindrome('Aa')).toEqual(1);
    expect(longestPalindrome('aA')).toEqual(1);
  });

  test('it returns 9 for a standard passing string', () => {
    expect(longestPalindrome('aaabbbccc')).toEqual(9);
  });

  test('it returns 9 for a standard passing string with jumbled chars', () => {
    expect(longestPalindrome('abbbaccac')).toEqual(9);
  });

  test('it returns 7 for a standard passing string with jumbled chars and mixed cases', () => {
    expect(longestPalindrome('Abbbaccac')).toEqual(9);
  });
});