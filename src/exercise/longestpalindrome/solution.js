/**
 * @param {string} s
 * @return {number}
 */
const longestPalindrome = function(s) {
  // for every char in string, check the object if it already has a key for it
  // if have, up the counter for that key
  // if no have, add the char as new key with count 1

  // set result to 0
  // for each key, if the key is odd and no odd key has been added, add count to result
  // if key is odd and an odd has already been added, add count-1 to result
  // if key is even, add count to result

  if (!s) return 0;
  if (s.length === 0) return 0;

  const charMap = {};
  for (let char of s) {
    if (!charMap[char]) charMap[char] = 1;
    else charMap[char]++;
  }

  console.log(charMap);

  let result = 0;
  let hasIncludedOdd = false;
  for (let char in charMap) {
    if (charMap[char] % 2 === 1) {
      if (!hasIncludedOdd) {
        result += charMap[char];
        hasIncludedOdd = true;
      }
      else result += charMap[char] - 1;
    } else {
      result += charMap[char];
    }
  }

  return result;
};

module.exports = {
  longestPalindrome
};