const functions = require('./index');

describe('longestCommonPrefix', () => {
  it('returns fl when given some strings with common prefix fl', () => {
    const longestPrefix = functions.longestCommonPrefix([
      'flower',
      'flow',
      'flight',
    ]);
    expect(longestPrefix).toBe('fl');
  });

  it('returns boo when given some strings with common prefix boo', () => {
    const longestPrefix = functions.longestCommonPrefix([
      'boooa',
      'booob',
      'boocc',
    ]);
    expect(longestPrefix).toBe('boo');
  });

  it('returns empty string when given some strings with no common prefix', () => {
    const longestPrefix = functions.longestCommonPrefix(['dog', 'racecar', 'car']);
    expect(longestPrefix).toBe('');
  });

  it('returns empty string when given empty array', () => {
    const longestPrefix = functions.longestCommonPrefix([]);
    expect(longestPrefix).toBe('');
  });

  // it('returns empty string when given array with at least 1 empty string', () => {
  //   const longestPrefix = functions.longestCommonPrefix([
  //     'flower',
  //     'flow',
  //     'flight',
  //     '',
  //   ]);
  //   expect(longestPrefix).toBe('');
  // });

  it('returns empty string when given array with at least 1 empty string', () => {
    const longestPrefix = functions.longestCommonPrefix(['abab', 'aba', '']);
    expect(longestPrefix).toBe('');
  });

  it('returns empty string when given non-empty strings that do not have any common prefixes', () => {
    const longestPrefix = functions.longestCommonPrefix(['acxxxa', 'cbxxxa']);
    expect(longestPrefix).toBe('');
  });
});

describe('doAllStringsHaveLetterAtPosition', () => {
  it('returns correctly when given array strings having some same some different', () => {
    const strings = [
      'flower',
      'flow',
      'flight',
    ];
    expect(functions.doAllStringsHaveLetterAtPosition(strings, 'f', 0)).toBeTruthy();
    expect(functions.doAllStringsHaveLetterAtPosition(strings, 'l', 1)).toBeTruthy();
    expect(functions.doAllStringsHaveLetterAtPosition(strings, 'o', 2)).toBeFalsy();
  });

  it('returns correctly when given array strings with at least 1 empty string', () => {
    const strings = [
      'flower',
      'flow',
      'flight',
      '',
    ];
    expect(functions.doAllStringsHaveLetterAtPosition(strings, 'f', 0)).toBeFalsy();
  });
});