function longestCommonPrefix(strings) {
  if (strings.length === 0) return '';

  const shortestString = getShortestString(strings);
  if (shortestString.length === 0) return '';

  let prefix = '';
  for (let i = 0; i < shortestString.length; i++) {
    const letter = shortestString[i];

    const allStringsHave = doAllStringsHaveLetterAtPosition(strings, letter, i);

    if (allStringsHave) prefix += letter;
    if (!allStringsHave) break;
  }

  return prefix;
}

function doAllStringsHaveLetterAtPosition(strings, letter, index) {
  for (let string of strings) {
    if (string[index] !== letter) return false;
  }

  return true;
}

function getShortestString(strings) {
  if (strings.length === 0) return '';

  let shortestString = strings[0];
  strings.forEach(string => {
    if (string.length < shortestString.length) shortestString = string;
  });

  return shortestString;
}

module.exports = {
  longestCommonPrefix,
  doAllStringsHaveLetterAtPosition,
};