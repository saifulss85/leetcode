const isValid = require('./solution');

describe('parentheses', () => {
  test('it should pass for ()', () => {
    expect(isValid('()')).toBeTruthy();
  });

  test('it should pass for []', () => {
    expect(isValid('[]')).toBeTruthy();
  });

  test('it should pass for {([])}', () => {
    expect(isValid('{([])}')).toBeTruthy();
  });

  test('it should pass for {([])}{}[]([{}])', () => {
    expect(isValid('{([])}{}[]([{}])')).toBeTruthy();
  });

  test('it should pass for empty string', () => {
    expect(isValid('')).toBeTruthy();
  });

  test('it should fail for bad string', () => {
    expect(isValid('[{)]')).toBeFalsy();
  });

  test('it should fail for bad string', () => {
    expect(isValid('[')).toBeFalsy();
  });
});