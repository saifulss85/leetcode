/**
 * @param {string} s
 * @return {boolean}
 */
class Stack {
  constructor() {
    this.data = [];
  }

  push(item) {
    this.data.push(item);
  }

  pop() {
    return this.data.pop();
  }

  peek() {
    return this.data[this.data.length - 1];
  }
}

const isValid = function(s) {
  // we'll create a stack
  // then we iterate through the string, one char at a time
  // each time we encounter an open bracket, we add it to the stack
  // each time we encounter a close bracket, we peek at the stack;
  // if stack has an open bracket of same type, we pop that and carry on to next char
  // if stack doesn't have, we return false
  // by end of string, return true

  const chars = s.split();

  const stack = new Stack();

  for (let char of s) {
    if (isOpenBracket(char)) stack.push(char);
    else if (isCloseBracket(char)) {
      const matchingOpeningBracket = getMatchingOpeningBracket(char);

      if (stack.pop() !== matchingOpeningBracket) return false;
    }

  }

  return !stack.peek();
};

function isOpenBracket(char) {
  switch (char) {
    case '(':
    case '[':
    case '{':
      return true;
    default:
      return false;
  }
}

function isCloseBracket(char) {
  switch (char) {
    case ')':
    case ']':
    case '}':
      return true;
    default:
      return false;
  }
}

function getMatchingOpeningBracket(bracket) {
  switch (bracket) {
    case ')':
      return '(';
    case ']':
      return '[';
    case '}':
      return '{';
    default:
      return false;
  }
}

module.exports = isValid;